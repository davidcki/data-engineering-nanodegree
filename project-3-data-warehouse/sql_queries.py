import configparser


# CONFIG
config = configparser.ConfigParser()
config.read('dwh.cfg')

# DROP TABLES

staging_events_table_drop = "DROP TABLE IF EXISTS staging_events"
staging_songs_table_drop = "DROP TABLE IF EXISTS staging_songs"
songplay_table_drop = "DROP TABLE IF EXISTS songplays"
user_table_drop = "DROP TABLE IF EXISTS users"
song_table_drop = "DROP TABLE IF EXISTS songs"
artist_table_drop = "DROP TABLE IF EXISTS artists"
time_table_drop = "DROP TABLE IF EXISTS time"

# CREATE TABLES

staging_events_table_create = ("""
CREATE TABLE IF NOT EXISTS staging_events (
    artist TEXT,
    auth TEXT,
    first_name TEXT,
    gender CHAR(1),
    item_in_session INT,
    last_name TEXT,
    length NUMERIC,
    level TEXT,
    location TEXT,
    method TEXT,
    page TEXT,
    registration BIGINT,
    session_id INT,
    song TEXT,
    status SMALLINT,
    ts BIGINT SORTKEY,
    user_agent TEXT,
    user_id INT
)
""")

staging_songs_table_create = ("""
CREATE TABLE IF NOT EXISTS staging_songs (
    song_id TEXT PRIMARY KEY,
    num_songs SMALLINT,
    title TEXT,
    artist_name TEXT,
    artist_latitude NUMERIC,
    year SMALLINT,
    duration NUMERIC,
    artist_id TEXT NOT NULL,
    artist_longitude NUMERIC,
    artist_location TEXT
) 
""")

songplay_table_create = ("""
CREATE TABLE IF NOT EXISTS songplays (
    songplay_id BIGINT IDENTITY(1,1) PRIMARY KEY,
    start_time BIGINT NOT NULL REFERENCES time (start_time),
    user_id INT NOT NULL REFERENCES users (user_id),
    level TEXT,
    song_id TEXT REFERENCES songs (song_id),
    artist_id TEXT REFERENCES artists (artist_id),
    session_id INT,
    location TEXT,
    user_agent TEXT
)
""")

user_table_create = ("""
CREATE TABLE IF NOT EXISTS users (
    user_id INT PRIMARY KEY,
    first_name TEXT,
    last_name TEXT,
    gender CHARACTER(1),
    level TEXT
)
""")

song_table_create = ("""
CREATE TABLE IF NOT EXISTS songs (
    song_id TEXT PRIMARY KEY,
    title TEXT,
    artist_id TEXT,
    year INT,
    duration NUMERIC
)
""")

artist_table_create = ("""
CREATE TABLE IF NOT EXISTS artists (
    artist_id TEXT PRIMARY KEY,
    name TEXT,
    location TEXT,
    latitude NUMERIC,
    longitude NUMERIC
)
""")

time_table_create = ("""
CREATE TABLE IF NOT EXISTS time (
    start_time BIGINT PRIMARY KEY,
    hour INT,
    day INT,
    week INT,
    month INT,
    year INT,
    weekday TEXT
)
""")

# STAGING TABLES

staging_events_copy = ("""
copy staging_events from 's3://udacity-dend/log_data'
credentials 'aws_iam_role={}'
json 's3://udacity-dend/log_json_path.json'
""").format(config.get("IAM_ROLE", "ARN"))

staging_songs_copy = ("""
copy staging_songs from 's3://udacity-dend/song_data'
credentials 'aws_iam_role={}'
json 'auto'
""").format(config.get("IAM_ROLE", "ARN"))

# FINAL TABLES

songplay_table_insert = ("""
INSERT INTO songplays (start_time, user_id, level, song_id, artist_id, session_id, location, user_agent)
SELECT e.ts, e.user_id, e.level, s.song_id, s.artist_id, e.session_id, e.location, e.user_agent
FROM staging_events e
LEFT JOIN staging_songs s ON e.song = s.title AND e.artist = s.artist_name AND e.length = s.duration
WHERE e.page = 'NextSong'
""")

user_table_insert = ("""
INSERT INTO users (user_id, first_name, last_name, gender, level)
SELECT DISTINCT user_id, first_name, last_name, gender, (SELECT level FROM staging_events WHERE user_id = X.user_id ORDER BY ts DESC LIMIT 1)
FROM (SELECT user_id, first_name, last_name, gender
FROM staging_events
WHERE user_id IS NOT NULL AND page = 'NextSong') X
""")

song_table_insert = ("""
INSERT INTO songs (song_id, title, artist_id, year, duration)
SELECT DISTINCT song_id, title, artist_id, year, duration
FROM staging_songs
""")

artist_table_insert = ("""
INSERT INTO artists (artist_id, name, location, latitude, longitude)
SELECT DISTINCT artist_id, artist_name, artist_location, artist_latitude, artist_longitude
FROM staging_songs
""")

time_table_insert = ("""
INSERT INTO time (start_time, hour, day, week, month, year, weekday)
SELECT DISTINCT ts, EXTRACT(hour FROM timestamp), EXTRACT(day FROM timestamp), EXTRACT(week FROM timestamp),
EXTRACT(month FROM timestamp), EXTRACT(year FROM timestamp),EXTRACT(dow FROM timestamp)
FROM (SELECT ts, TIMESTAMP 'epoch' + ts * INTERVAL '0.001 second' AS timestamp FROM staging_events WHERE page = 'NextSong')
""")

# QUERY LISTS

create_table_queries = [staging_events_table_create, staging_songs_table_create, user_table_create, song_table_create, artist_table_create, time_table_create, songplay_table_create]
drop_table_queries = [staging_events_table_drop, staging_songs_table_drop, songplay_table_drop, user_table_drop, song_table_drop, artist_table_drop, time_table_drop]
copy_table_queries = [staging_events_copy, staging_songs_copy]
insert_table_queries = [user_table_insert, song_table_insert, artist_table_insert, time_table_insert, songplay_table_insert]
