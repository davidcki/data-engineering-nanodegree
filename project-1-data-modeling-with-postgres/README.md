# Project: Data Modeling with Postgres

## Introduction
A startup called Sparkify wants to analyze the data they've been collecting on songs and user activity on their new
music streaming app. The analytics team is particularly interested in understanding what songs users are listening to. 
Currently, they don't have an easy way to query their data, which resides in a directory of JSON logs on user activity 
on the app, as well as a directory with JSON metadata on the songs in their app.

They'd like a data engineer to create a Postgres database with tables designed to optimize queries on song play 
analysis, and bring you on the project. Your role is to create a database schema and ETL pipeline for this analysis. 
You'll be able to test your database and ETL pipeline by running queries given to you by the analytics team from 
Sparkify and compare your results with their expected results.

## Project structure
| File   | Description      | 
|--------|------------------|
| song_data/ | directory with JSON files that contain song's data |
| log_data/ | directory with JSON files that contain data describing users' activity |
| test.ipynb | displays the first few rows of each table to let you check your database |
| create_tables.py | drops and creates your tables. You run this file to reset your tables before each time you run your ETL scripts |
| etl.ipynb | reads and processes a single file from song_data and log_data and loads the data into your tables. This notebook contains detailed instructions on the ETL process for each of the tables |
| etl.py | reads and processes files from song_data and log_data and loads them into your tables |
| sql_queries.py | contains all your sql queries, and is imported into the last three files above |

## Database
To run Postgres in Docker
```
docker run --rm --name c-postgres -e POSTGRES_USER=student -e POSTGRES_PASSWORD=student -e POSTGRES_DB=studentdb -d -p 5432:5432 postgres
```

## Executing the project
1. Run ```python create_tables.py``` 
2. Run ```python etl.py```

## Data model

### Model
The star schema for Sparkify's analytics
![Sparkify Model](sparkify_model.png)

### Model description
The fact table songplays contains the events that represent user activities in the app regarding songs streaming.

Dimension tables users, time, songs and artists expand the data in the fact table in order to further better categorize the events.

## Analytics goals
The model allows measuring songs reproductions, obtaining immediate insights about the location where those songs 
(and the artists) are being listened from, the kind of devices used to stream music, the behavior of paid vs free accounts,
among other. The analysis can be potentially used to recommend songs and artists to users based on their musical taste,
inform a user about local events that include their favorite artists, optimize the content and user experience for specific
devices, etc.

## Analytics examples
```
SELECT
	user_agent ,
	COUNT(*)
FROM
	songplays
GROUP BY
	user_agent
ORDER BY
	COUNT(*) DESC;
```
Shows that the most common device used for streaming in Sparkify is Chrome 36 on Mac OS X (Maverick), while the least
common is Internet Explorer 11 on Windows 7.

```
SELECT
	level,
	(count::DECIMAL / (SELECT COUNT(*) FROM songplays)) * 100 AS percentage
FROM
	(
	SELECT
		LEVEL,
		COUNT(*) AS count
	FROM
		songplays
	GROUP BY
		LEVEL
	ORDER BY
		count DESC) AS X;
```
Indicates that 82% of the processed streaming events came from paid accounts.